package io.sebradloff;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    
    @Test
    public void add_shouldReturnSumOfTwoNumbers(){
        //given
        Calculator calculator = new Calculator();
        //when
        double actual = calculator.add(1, 2);
        //then
        // assertEquals takes your expected, actual, and "fuzz factor"
        assertEquals(3.0, actual, 0.001);
    }

    @Test
    public void addAll_shouldReturnSumOfMultipleNumbers() {
        //given
        Calculator calculator = new Calculator();
        double[] nums = {1.0,2.0,3.2};
        //when
        double actual = calculator.addAll(nums);
        //then
        assertEquals(6.2, actual, 0.001);


    }

    @Test
    public void subtract_shouldReturnDifferenceOfTwoNumbers() {
        Calculator calculator = new Calculator();
        double actual = calculator.subtract(10,5);

        assertEquals(5.0, actual, 0.001);
    }

    @Test
    public void subtract_shouldReturnDifferenceOfTwoDoubles() {
        Calculator calculator = new Calculator();

        double actual = calculator.subtract(1.0, 20.0);

        assertEquals(-19.0, actual, 0.001);
    }


    @Test
    public void multiply_shouldReturnTheProductOfTwoNumbers() {
        Calculator calculator = new Calculator();

        double actual = calculator.multiply(4.0, 5.0);

        assertEquals(20.0, actual, 0.001);
    }

    @Test
    public void mod_shouldReturnTheDivdedendAndDivisorRemainder() {
        Calculator calculator = new Calculator();

        double actual = calculator.mod(8.0, 3.0);

        assertEquals(2.0, actual, 0.001);
    }


}
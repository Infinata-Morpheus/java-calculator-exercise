package io.sebradloff;

import java.lang.reflect.Array;

public class Calculator {

    public double add(int num1, int num2) {

        return num1 + num2;
    }

    public double addAll(double[] nums) {
        double sumNums = 0;
        for(int i = 0; i < nums.length; i++) {
            sumNums += nums[i];
        }

        return sumNums;
    }

    public double subtract(double num1, double num2) {
        return num1 - num2;
    }

    public double multiply(double operand1, double operand2) {

         return operand1 * operand2;
    }

    public double mod(double v, double v1) {
        return 0;
    }


    // write a method that subtracts one number from another and returns the difference

    // write a method that multiplies one number with another and returns the product

    // write a method that calculates the remainder between the dividend and the divisor

    // write a method that tells me if a number is odd

    // write a method that tells me if a number is even

    // write a method that tells me if a number is odd or even, by printing out the answer to the console

    // I want the calculator to have a name when I create it

    // I want the calculator to introduce itself to me

    // I want the calculator to let me guess what the addition of two numbers is and tell me if I'm right or wrong so that I can improve my math skills
}
